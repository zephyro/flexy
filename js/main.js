
// Header Menu

$(function() {
    var pull = $('.topnav-toggle');
    var menu = $(pull.attr("data-target"));

    $(pull).on('click', function(e) {
        e.preventDefault();
        menu.slideToggle(200);
        pull.toggleClass('open');
    });

    $(window).resize(function(){
        var w = $(window).width();
        if(w > 768 && menu.is(':hidden')) {
            menu.removeAttr('style');
            pull.removeClass('open');
        }
    });

    $('body').click(function (event) {
        var w = $(window).width();
        if((w < 768) && ($(event.target).closest(".topnav").length === 0) && ($(event.target).closest(".topnav-toggle").length === 0)) {
            $(".topnav-toggle").removeClass('open');
            $(".topnav").hide(100);
        }
    });
});


// Header Contact

$(function() {
    var pull = $('.contact-toggle');
    var menu = $(pull.attr("data-target"));

    $(pull).on('click', function(e) {
        e.preventDefault();
        menu.slideToggle(200);
        pull.toggleClass('open');
    });

    $(window).resize(function(){
        var w = $(window).width();
        if(w > 768 && menu.is(':hidden')) {
            menu.removeAttr('style');
            pull.removeClass('open');
        }
    });

    $('body').click(function (event) {
        var w = $(window).width();

        if((w < 768) && ($(event.target).closest(".header-contact").length === 0) && ($(event.target).closest(".contact-toggle").length === 0) ){

            $(".contact-toggle").removeClass('open');
            $(".header-contact").hide(100);
        }
    });
});


$('.cocoen').cocoen();

// $("input[type='radio']").ionCheckRadio();
$("input[type='checkbox']").ionCheckRadio();

$(".btn-modal").fancybox({
    'padding'    : 0
});

$(".modal-close").on('click', function(e) {
    e.preventDefault();
    $.fancybox.close();
});


// Review Slider

$('.review-slider').slick({
    dots: false,
    arrows: true,
    infinite: true,
    speed: 300,
    autoplay: true,
    autoplaySpeed: 5000,
    slidesToShow: 2,
    slidesToScroll: 1,
    prevArrow: '<span class="slide-nav prev"></span>',
    nextArrow: '<span class="slide-nav next"></span>',
    responsive: [
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
});


// Gallery Slider

$('.gallery-slider').slick({
    dots: false,
    arrows: true,
    infinite: true,
    speed: 300,
    autoplay: true,
    autoplaySpeed: 5000,
    slidesToShow: 2,
    slidesToScroll: 1,
    prevArrow: '<span class="slide-nav prev"></span>',
    nextArrow: '<span class="slide-nav next"></span>',
    responsive: [
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
});

$('.form-select').selectric({
    disableOnMobile: false,
    responsive: true,
    maxHeight: 350
});
